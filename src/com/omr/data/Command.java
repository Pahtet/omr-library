package com.omr.data;

import java.io.Serializable;

public class Command implements Serializable
{
	private CommandsEnum command;
	private Object data;
	
	public Command(CommandsEnum command, Object data)
	{
		this.command=command;
		this.data=data;
	}
	
	public CommandsEnum getCommand()
	{
		return command;
	}
	
	public Object getData()
	{
		return data;
	}

}
