package com.omr.data;

import java.io.Serializable;

public enum CommandsEnum implements Serializable
{
	PASSWORD,
	GETMEMORYUSAGE,
	MEMORYUSAGE,
	RUNCOMMAND,
	ACTIVATE,
	DEACTIVATE,
	CONNECT, 
	VERSION,
	CODESEGMENT,
	CODEMETRICS;
}
