package com.omr.data;

import java.io.Serializable;
import java.util.Scanner;

public class ProcessInfo implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String processName;
	private Integer pid;
	private String tty;
	private Long memory;		

	public boolean parseProcessStatus(String in)
	{
		Scanner s=new Scanner(in);
		String data[] =new String[4];
		for(int i=0;i<4;i++)
		{
			if(s.hasNext())
			{
				data[i]=s.next();
			}
			else
			{
				s.close();
				return false;
			}
			
		}
		s.close();
		processName = data[0];
		parsePid(data[1]);
		tty = data[2];
		parseMemory((data[3]));
		return true;		
	}
	
	public boolean parseTaskList(String in)
	{
		in = in.replaceAll("\"", ""); 
		String[] data = in.split(",");
		if (data.length != 5)
			return false;		
		processName = data[0];	
		parsePid(data[1]);
		tty = data[2];
		parseMemory(data[4]);		
		return true;
	}

	private boolean parsePid(String in)
	{
		try
		{
			pid = Integer.parseInt(in);
		}
		catch (NumberFormatException e)
		{
			pid = null;
			return false;
		}
		return true;
	}
	
	private boolean parseMemory(String in)
	{
		in = in.replaceAll("\\D",""); 
		try
		{
			memory = Long.parseLong(in);
		}
		catch (NumberFormatException e)
		{
			memory = null;
			return false;
		}
		return true;
	}
	
	public boolean isValid()
	{	
		Object arrayData[] = {processName, pid, tty, memory};
		for (Object x: arrayData)
		{
			if (x == null)
				return false;
		}
		return true;
	}
	
	public String toString()
	{
		return processName + " " + pid + " " + memory;
	}

	public String getProcessName()
	{
		return processName;
	}

	public int getPid()
	{
		return pid;
	}

	public String getTty()
	{
		return tty;
	}

	public long getMemory()
	{
		return memory;
	}
}
