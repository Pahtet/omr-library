package com.omr.data.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.omr.data.ProcessInfo;

public class ProcessInfoTest
{

	@Test
	public void testBlank()
	{
		ProcessInfo pi=new ProcessInfo();
		pi.parseTaskList("");
		assertFalse(pi.isValid());
	}
	
	@Test
	public void testOneArg()
	{
		ProcessInfo pi=new ProcessInfo();
		pi.parseTaskList("System");
		assertFalse(pi.isValid());
	}
	
	@Test
	public void testValid()
	{
		ProcessInfo pi=new ProcessInfo(); 
		pi.parseTaskList("\"chrome.exe\",\"4024\",\"Console\",\"1\",\"109 048 ��\""); //Fix
		assertTrue(pi.isValid());
	}
	
	@Test
	public void testNotValid()
	{
		ProcessInfo pi=new ProcessInfo();
		pi.parseTaskList("\"chrome.exe\",\"test\",\"Console\",\"test\",\"-1\"");
		assertFalse(pi.isValid());
	}
	
	@Test
	public void testPSheader()
	{
		ProcessInfo pi=new ProcessInfo();
		pi.parseProcessStatus("COMMAND           PID TT        SIZE");
		assertFalse(pi.isValid());
	}
	
	@Test
	public void testPSValid()
	{
		ProcessInfo pi=new ProcessInfo();
		pi.parseProcessStatus("init 1 ? 324");
		assertTrue(pi.isValid());
	}
}
