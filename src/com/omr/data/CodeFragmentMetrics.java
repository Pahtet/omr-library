package com.omr.data;

public class CodeFragmentMetrics
{
	private int  codeFragmentId;
	private int time;
	
	public CodeFragmentMetrics() {
		// TODO Auto-generated constructor stub
	}

	public int getCodeFragmentId() {
		return codeFragmentId;
	}

	public void setCodeFragmentId(int codeFragmentId) {
		this.codeFragmentId = codeFragmentId;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

}
